RCOMP 2021-2022 Project - Sprint 2 review
=========================================
### Sprint master: 1200876 ###
(This file is to be created/edited by the sprint master only)
# 1. Sprint's backlog #
In this second sprint the team will create a network simulation matching the developed structured cabling
project of the first sprint. In this sprint the focus is on the layer two infrastructure, and the layer three
fundamentals (IPv4 addressing, and static routing).
For such purposes, the Cisco Packet Tracer is the tool will be used.

# 2. Subtasks assessment #

## 2.1. 1200829 -Network simulation for Building 2 #
### Totally implemented with no issues. ###
## 2.2. 1200876 - Network simulation for Building 1 #
### Totally implemented with no issues. ###
## 2.3. 1200873 - Network simulation for Building 4 #
### Totally implemented with no issues. ###
## 2.4. 1210829 - Network simulation for Building 5 #
### Totally implemented with no issues. ###
## 2.5. 1200884 - Network simulation for Building 3 #
### Totally implemented with no issues. ###
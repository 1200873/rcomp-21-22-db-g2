!
version 12.4
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Router
!
!
!
!
!
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 no ip address
 duplex auto
 speed auto
!
interface FastEthernet0/0.170
 encapsulation dot1Q 170
 ip address 172.16.88.5 255.255.255.128
!
interface FastEthernet0/0.191
 encapsulation dot1Q 191
 ip address 172.16.95.1 255.255.255.192
!
interface FastEthernet0/0.192
 encapsulation dot1Q 192
 ip address 172.16.95.65 255.255.255.192
!
interface FastEthernet0/0.193
 encapsulation dot1Q 193
 ip address 172.16.95.129 255.255.255.192
!
interface FastEthernet0/0.194
 encapsulation dot1Q 194
 ip address 172.16.95.193 255.255.255.224
!
interface FastEthernet0/0.195
 encapsulation dot1Q 195
 ip address 172.16.95.225 255.255.255.224
!
interface FastEthernet0/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface Vlan1
 no ip address
 shutdown
!
ip classless
ip route 172.16.92.0 255.255.255.0 172.16.88.2 
ip route 172.16.93.0 255.255.255.0 172.16.88.3 
ip route 172.16.94.0 255.255.255.0 172.16.88.4 
ip route 0.0.0.0 0.0.0.0 172.16.88.1 
!
ip flow-export version 9
!
!
!
!
!
!
!
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end


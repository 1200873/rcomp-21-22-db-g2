RCOMP 2021-2022 Project - Sprint 2 - Member 1210829 folder
===========================================
                            Routing Table
Network Address            Network Mask           Next hop
172.16.92.0                255.255.255.0          172.16.88.2
172.16.93.0                255.255.255.0          172.16.88.3
172.16.94.0                255.255.255.0          172.16.88.4
0.0.0.0                    0.0.0.0                172.16.88.1
172.16.95.0               255.255.255.0           172.16.88.5


In Building 5 the IPV4 Address is 172.16.95.0/26 and this address was divided in 5 addresses because it is necessary for the Vlans


Vlan Address              Vlan Mask               Vlan Name
172.16.95.0               255.255.255.192         WI-FI
172.16.95.64              255.255.255.192         Floor1
172.16.95.128             255.255.255.192         Floor0
172.16.95.192             255.255.255.224         DMZ
176.16.95.224             255.255.255.224         VoIP




RCOMP 2021-2022 Project - Sprint 2 - Member 1200884 folder
===========================================
On the third Building the starting IPv4 Address -> 172.16.93.0/26, /26 due to the fact that the wi-fi network needed 55 nodes, rounded up to 64(2^6), as such, 32-6=26, which means 6 bits from the Ip would be reserved to the hosts. Thus the mask is 255.255.255.192.
This means the next Ip available would be 172.16.93.64/26, directed to the First floor. /26 is for the 45 nodes that are needed. The mask is 255.255.255.192.
After those 64 hosts the next Ip available is 172.16.93.128/26, directed to the Floor 0. /26 is for the 35 nodes that are needed. The Mask is 255.255.255.192.
The next starting IPv4 Address is 172.16.93.192/27, directed to the DMZ. /27 is for the 28, rounded up to 32 nodes that are needed. (2^5) 32-5=27. The mask is 255.255.255.224.
The final IPv4 Address is 172.16.93.224/27, directed to the VoIP. /27 is for the 25 nodes needed. The mask is 255.255.255.224.
In this folder it is possible to open the image of the layout of the project as well as its file, in which the Building 3 is represented and the excerpt of the other buildings, in which the other ones are represented by their ICC and Router. The complete building is in its respective folder.
After changing the ICC connections to trunk mode, the domain was configured so that the switches communicate between the various VLANs, and the configuration to client mode.
Next, the VLANs were configured in each HCC, 181-DMZ, 182-VoIP, 183-Wi-fi Network, 184-Floor 0, 185-Floor 1.
Finally, the last step was giving the routers the next IP after the starting one stated in the first part of the report, and this way, each Router had all the information needed for each VLAN.
RCOMP 2021-2022 Project - Sprint 2 planning
===========================================
### Sprint master: 1200876 ###
(This file is to be created/edited by the sprint master only)
# 1. Sprint's backlog #
In this second sprint the team will create a network simulation matching the developed structured cabling
project of the first sprint. In this sprint the focus is on the layer two infrastructure, and the layer three
fundamentals (IPv4 addressing, and static routing).
For such purposes, the Cisco Packet Tracer is the tool will be used.
# 2. Technical decisions and coordination #
We decided to put 2 access points in each building, one for each floor, one pc per floor and only one server for each building.
Naming conventions:
  * Switch: XXX_SWITCH_BY_FZ
  * PC: PC_BY_FZ
  * Access point: ACCESS_POINT_BY_FZ
  * Server: SERVER_BY
  * IP Phone: PHONE_BY_FZ
  * Laptop: LAPTOP_BY_FZ
  
X- Type of switch (MC, ICC, HCC)
Y- Building number
Z- Number of the floor

| VLANIDs | Name | IP Address | 
|:-------------  |:-------------- |:--------------|
| 170 	 |	Backbone | 172.16.88.0/25   |  
| 171 	 |	GroundFloor_B1 | 172.16.90.0/26   |  
| 172 	 |	SecondFloor_B1 | 172.16.89.128/25   |  
| 173 	 |	Wi-Fi_B1 | 172.16.88.128/25   |  
| 174 	 |	DMZ_B1 	  | 172.16.89.0/25   |  
| 175 	 |	VolP_B1 	  | 172.16.90.64/26   |  
| 176 	 |	GroundFloor_B2 	  | 172.16.92.192/27   |  
| 177 	 |	Wi-Fi_B2 	  | 172.16.92.0/25   |  
| 178 	 |	DMZ_B2 	  | 172.16.92.224/28   |  
| 179 	 |	VoIP_B2 	  | 172.16.92.240/28   |  
| 180 	 |	SecondFloor_B2 	  | 172.16.92.128/26   |  
| 181 	 |	DMZ_B3 	  | 172.16.93.192/27   |  
| 182 	 |	VoIP_B3 	  | 172.16.93.224/27   |  
| 183 	 |	Wifi_Network_B3 	  | 172.16.93.0/26   |  
| 184 	 |	Floor0_B3 	  | 172.16.93.128/26   |  
| 185 	 |	Floor1_B3 	  | 172.16.93.64/26   |  
| 186 	 |	wifi_network_B4 	  | 172.16.94.0/25;   |  
| 187 	 |	floor1_B4 	  | 172.16.94.128/26;   |  
| 188 	 |	floor0_B4 	  | 172.16.94.192/27   |  
| 189 	 |	dmz_B4 	  | 172.16.94.224/28   |  
| 190 	 |	voip_B4 	  | 172.16.94.240/28   |  
| 191 	 |	Server_B5 	  | 172.16.95.192/27   |  
| 192 	 |	WiFi_B5 	  | 172.16.95.0/26   |  
| 193 	 |	Floor1_B5 	  | 172.16.95.64/26   |  
| 194 	 |	Floor0_B5 	  | 172.16.95.128/26   |  
| 195 	 |	voIP_B5 	  | 176.16.95.224/27   |  

The routing protocols are:

Building 1->ip route 0.0.0.0 0.0.0.0 172.16.88.1
Building 2->ip route 172.16.92.0 255.255.255.0 172.16.88.2
Building 3->ip route 172.16.93.0 255.255.255.0 172.16.88.3
Building 4->ip route 172.16.94.0 255.255.255.0 172.16.88.4
Building 5->ip route 172.16.95.0 255.255.255.0 172.16.88.5

The initial IPV4 Address was divided into 5 addresses, one for each building.
For this, we calculated the amount of nodes each building needed by summing the nodes of each Vlan.To be sure that the mask we choose is enough for the building we rounded up the amount of nodes of each Vlan to the closest base power of 2, for example ( 45 nodes would count as 64, because 2^6=64)

After all the calculations, the amount of nodes of each building were:
Building 1->1024 nodes
Building 2->256 nodes 
Building 3->256 nodes 
Building 4->256 nodes
Building 5->256 nodes 

For the first building nodes we will need 22 bits for the network and for all others we will need 24.
The final IPV4 addresses were:
B1->172.16.88.0/22
B2->172.16.92.0/24
B3->172.16.93.0/24
B4->172.16.94.0/24
B5->172.16.95.0/24

# 3. Subtasks assignment #
Each team member was assigned the building from the previous sprint, doing the simulation on Packet Tracer for the switches, routers, ips, etc...
As Sprint Master, student number 1200876 was assigned the extra task of assembling all the simulations ont one big simulation and making this document.

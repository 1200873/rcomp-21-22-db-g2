!
version 15.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Router
!
!
!
!
!
!
!
!
ip cef
no ipv6 cef
!
!
!
!
license udi pid CISCO2811/K9 sn FTX1017OMOO-
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 no ip address
 duplex auto
 speed auto
!
interface FastEthernet0/0.170
 encapsulation dot1Q 170
 ip address 172.16.88.1 255.255.255.128
!
interface FastEthernet0/0.171
 encapsulation dot1Q 171
 ip address 172.16.90.2 255.255.255.192
!
interface FastEthernet0/0.172
 encapsulation dot1Q 172
 ip address 172.16.89.130 255.255.255.128
!
interface FastEthernet0/0.173
 encapsulation dot1Q 173
 ip address 172.16.88.130 255.255.255.128
!
interface FastEthernet0/0.174
 encapsulation dot1Q 174
 ip address 172.16.89.2 255.255.255.128
!
interface FastEthernet0/0.175
 encapsulation dot1Q 175
 ip address 172.16.90.66 255.255.255.192
!
interface FastEthernet0/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface Vlan1
 no ip address
 shutdown
!
ip classless
ip route 172.16.92.0 255.255.255.0 172.16.88.2 
ip route 172.16.93.0 255.255.255.0 172.16.88.3 
ip route 172.16.94.0 255.255.255.0 172.16.88.4 
ip route 172.16.95.0 255.255.255.0 172.16.88.5 
!
ip flow-export version 9
!
!
!
no cdp run
!
!
!
!
!
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end


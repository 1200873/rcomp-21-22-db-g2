RCOMP 2021-2022 Project - Sprint 1 - Member 1200873 folder
===========================================
(This folder is to be created/edited by the team member 1200873 only)

On the forth Building the starting IPv4 Address -> 172.16.94.0/25, /25 due to the fact that the wi-fi network needed 70 nodes, rounded up to 128(2^7), as such, 32-7=25, which means 7 bits from the Ip are reserved to the hosts. Thus the mask is 255.255.255.128.
This means the next Ip available is 172.16.94.128/26, directed to the Floor 1. /26 is for the 55 nodes that are needed. The mask is 255.255.255.192.
The next Ip available is 172.16.94.192/27, directed to the Floor 0. /27 is for the 28 nodes that are needed. The Mask is 255.255.255.224.
The next starting IPv4 Address is 172.16.94.224/28, directed to the DMZ. /28 is for the 10 nodes, rounded up to 16 nodes, that are needed. (2^4) 32-4=28. The mask is 255.255.255.240.
The final IPv4 Address is 172.16.94.240/28, directed to the VoIP. /28 is for the 12 nodes needed. The mask is 255.255.255.240.
In this folder it is possible to open the image of the layout of the project as well as its file, in which the Building 4 is represented and excerpts of the other buildings, in which the other ones are represented by their ICC and Router. The complete building is in its respective folder.
After changing the ICC connections to trunk mode, the domain was configured so that the switches communicate between the various VLANs, and the configuration to client mode.
Next, the VLANs were configured in each HCC, 189-DMZ, 190-VoIP, 186-Wi-fi Network, 188-Floor 0, 187-Floor 1.
Finally, the last step was giving the routers the next IP after the starting one stated in the first part of the report, and this way, each Router had all the information needed for each VLAN.

RCOMP 2021-2022 Project - Sprint 1 - Member 1200876 folder
===========================================
(This folder is to be created/edited by the team member 1200876 only)

In this Sprint I was in charge of the network simulation of the first building.I created 1 MC, 1 ICC and 2 HCC(one for each floor),even though I only have one HCC for both buildings in my developed structured cabling
project of the first sprint.Then,after completing my building, I added the ICC and the router of all other buildings to be able to create a routing table.I programmed the MC for the full structure of all the buildings.
I also programed all routers by adding the corresponding Vlans Id of the building and an ip addres to be our default gateway. 

On my Building the starting IPv4 Address is 172.16.88.0/22. This addres is going to be divided into 5 minor addresses for each one of the Vlans.

| Vlan Address| Vlan Mask   | Vlan Name      | 
|:-------------  |:-------------- |:--------------|
| 172.16.88.0    |	255.255.255.128 | Backbone |
| 172.16.88.128	 |	255.255.255.128 | Wi-fi Network |  
| 172.16.89.128  |	255.255.255.128 | Floor 1|  
| 172.16.90.0	 |	255.255.255.192 | Floor 0 |  
| 172.16.89.0	 |	255.255.255.128 | DMZ|  
| 172.16.90.64	 |	255.255.255.192 | VoIP|

As its possible to see, the Vlans are organized from the Vlan with more to less users. This way we can be sure that all Vlans will fit into the space provided.

**Routing Table**

| Network Address| Network Mask   | Next Hop      | 
|:-------------  |:-------------- |:--------------|
| 172.16.92.0 	 |	255.255.255.0 | 172.16.88.2   |
| 172.16.93.0 	 |	255.255.255.0 | 172.16.88.3   |
| 172.16.94.0 	 |	255.255.255.0 | 172.16.88.4   |
| 172.16.95.0 	 |	255.255.255.0 | 172.16.88.5   |

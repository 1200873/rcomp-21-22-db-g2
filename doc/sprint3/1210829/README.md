RCOMP 2021-2022 Project - Sprint 3 - Member 1210829 folder
===========================================
(This folder is to be created/edited by the team member 1210829 only)

#### 1. OSPF Dynamic Routing ####

do sh run | i ip route
network 172.16.88.5 0.0.0.0 area 0
network 172.16.95.0 0.0.0.0 area 5
network 172.16.95.64 0.0.0.0 area 5
network 172.16.95.128 0.0.0.0 area 5
network 172.16.95.192 0.0.0.0 area 5
network 172.16.95.224 0.0.0.0 area 5


## 2. HTTP SERVERS

this step i created a new server and added an ip address to him. Then i edited the index.html.

## 3. DHCPv4 SERVICE

ip dhcp pool F0
network 172.16.95.127 255.255.255.192
default-router 172.16.95.128

ip dhcp pool F1
network 172.16.95.63 255.255.255.192
default-router 172.16.95.64

ip dhcp pool WI-FI
network 172.16.95.0 255.255.255.192
default-router 172.16.95.1

ip dhcp pool voIP
network 172.16.95.223 255.255.255.224
default-router 172.16.95.224
option 150 ip 172.16.95.224

## 4. VoIP SERVICE

telephony-service
auto-reg-ephone
ip source-address 172.16.95.224 port 2000
max-ephones 20
max-dn 20
auto assign 4 to 6
auto assign 1 to 5
ephone-dn 1
number 50001
ephone-dn 2
number 50002


dial-peer voice 1 voip
destination-pattern 1....
session target ipv4:172.16.90.66

dial-peer voice 2 voip
destination-pattern 2....
session target ipv4:172.16.92.241

dial-peer voice 3 voip
destination-pattern 3...
session target ipv4:172.16.93.225

dial-peer voice 4 voip
destination-pattern 4....
session target ipv4:172.16.94.241

## 5. DNS

ip dhcp pool F0
dns-server 172.16.95.2
exit
ip dhcp pool F1
dns-server 172.16.95.2
exit
ip dhcp pool WI-FI
dns-server 172.16.95.2
exit
ip dhcp pool voIP
dns-server 172.16.95.2
exit


## 6. NAT

int f0/0.170
ip nat outside
int f0/0.191
ip nat inside
int f0/0.192
ip nat inside
int f0/0.193
ip nat inside
int f0/0.194
ip nat inside
int f0/0.195
ip nat inside



ip nat inside source static tcp 172.16.88.5 80 172.16.95.2 80
ip nat inside source static tcp 172.16.88.5 443 172.16.95.2 443
ip nat inside source static tcp 172.16.88.5 53 172.16.95.3 53
ip nat inside source static udp 172.16.88.5 53 172.16.95.3 53


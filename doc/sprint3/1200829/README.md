RCOMP 2021-2022 Project - Sprint 3 - Member 1200829 folder
===========================================
##1. OSPF DYNAMIC ROUTING##

do sh run | i ip route 
delete static routing table.
router ospf 1
do sh ip int bri
network 172.16.88.2 0.0.0.0 area 0
network 172.16.92.193 0.0.0.0 area 2
network 172.16.92.1 0.0.0.0 area 2
network 172.16.92.225 0.0.0.0 area 2
network 172.16.92.241 0.0.0.0 area 2
network 172.16.92.129 0.0.0.0 area 2

##2.HTTP SERVERS##

in this step i created a new server and added an ip addres to him. Then i edited the index.html.

##3.DHCPv4 SERVICE##
ip dhcp excluded-address 172.16.92.193
ip dhcp excluded-address 172.16.92.1
ip dhcp excluded-address 172.16.92.129
ip dhcp excluded-address 172.16.92.241

ip dhcp pool NET-2-176
 network 172.16.92.192 255.255.255.224
 default-router 172.16.92.193
 dns-server 172.16.92.230
 
ip dhcp pool NET-2-177
 network 172.16.92.0 255.255.255.128
 default-router 172.16.92.1
 dns-server 172.16.92.230
 
ip dhcp pool NET-2-180
 network 172.16.92.128 255.255.255.192
 default-router 172.16.92.129
 dns-server 172.16.92.230
 
ip dhcp pool VOICE-2
 network 172.16.92.240 255.255.255.240
 option 150 ip 172.16.92.241
 dns-server 172.16.92.230

##4.VOIP SERVICE##

telephony-service
 max-ephones 10
 max-dn 10
 ip source-address 172.16.93.241 port 2000

ephone-dn 3
 number 20001

ephone-dn 4
 number 20002

ephone 3
 mac-address 00D0.BA8A.C7AA
 type 7960
 button 1:3

ephone 4
 mac-address 0060.705C.C738
 type 7960
 button 1:4


##5.DNS##
ip dhcp pool NET-2-176 
dns-server 172.16.92.230
exit

ip dhcp pool NET-2-177 
dns-server 172.16.92.230
exit

ip dhcp pool NET-2-179 
dns-server 172.16.92.230
exit

ip dhcp pool NET-2-180 
dns-server 172.16.92.230
exit

I also created a DNS table in my server.

##6.NAT##

ip nat inside source static tcp 172.16.88.2 80 172.16.92.227 80 
ip nat inside source static tcp 172.16.88.2 443 172.16.92.227 443 
ip nat inside source static tcp 172.16.88.2 53 172.16.92.230 53 
ip nat inside source static udp 172.16.88.2 53 172.16.92.230 53
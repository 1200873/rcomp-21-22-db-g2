RCOMP 2021-2022 Project - Sprint 1 - Member 1200873 folder
===========================================
(This folder is to be created/edited by the team member 1200873 only)

#### This is just an example for a team member with number 1200873 ####
### Each member should create a folder similar to this, matching his/her number. ###
The owner of this folder (member number 1200873) will commit here all the outcomes (results/artifacts/products).

## 1. OSPF DYNAMIC ROUTING

* do sh run | i ip route 
* no ip route 172.16.92.0 255.255.255.0 172.16.88.2 
* no ip route 172.16.93.0 255.255.255.0 172.16.88.3 
* no ip route 172.16.95.0 255.255.255.0 172.16.88.5 
* no ip route 0.0.0.0 0.0.0.0 172.16.88.1 
* router ospf 1
* do sh ip int bri
* network 172.16.88.4 0.0.0.0 area 0
* network 172.16.94.193 0.0.0.0 area 4
* network 172.16.94.1 0.0.0.0 area 4
* network 172.16.94.255 0.0.0.0 area 4
* network 172.16.94.241 0.0.0.0 area 4
* network 172.16.94.129 0.0.0.0 area 4

## 2. HTTP SERVERS

acrescentar um server
ligar á vlan DMZ
acrescentar um ip address
ativar http
editar o ficheiro 5

## 3. DHCPv4 SERVICE

no HCC ligado ao telemovel fazer: 
switchport mode access
switchport voice vlan 190
no switchport access vlan
----------------------------------------WIFI NTEWORK
ip dhcp excluded-address 172.16.94.1
ip dhcp pool NET-4-186
network 172.16.94.0 255.255.255.128
default-router 172.16.94.1
----------------------------------------FLOOR1
ip dhcp excluded-address 172.16.94.129
ip dhcp pool NET-4-187
network 172.16.94.128 255.255.255.192
default-router 172.16.94.129
----------------------------------------FLOOR0
ip dhcp excluded-address 172.16.94.193
ip dhcp pool NET-4-188
network 172.16.94.192 255.255.255.224
default-router 172.16.94.193
----------------------------------------DMZ
ip dhcp excluded-address 172.16.94.225
ip dhcp pool NET-4-189
network 172.16.94.224 255.255.255.240
default-router 172.16.94.225
----------------------------------------VoIP
ip dhcp excluded-address 172.16.94.241
ip dhcp pool VOICE-4
network 172.16.94.240 255.255.255.240
default-router 172.16.94.241
option 150 ip 172.16.94.241

## 4. VoIP SERVICE

telephony-service
auto-reg-ephone
ip source-address 172.16.94.241 port 2000
max-ephones 20
max-dn 20
auto assign 4 to 6
auto assign 1 to 5
ephone-dn 1
number 40001
ephone-dn 2
number 40002

## 5. DNS

ip dhcp pool NET-4-186 
dns-server 172.16.94.226
exit
ip dhcp pool NET-4-187
dns-server 172.16.94.226
exit
ip dhcp pool NET-4-188
dns-server 172.16.94.226
exit
ip dhcp pool VOICE-4
dns-server 172.16.94.226
exit

## 6. NAT 
ip nat inside source static tcp 172.16.88.4 80 172.16.94.227 80
ip nat inside source static tcp 172.16.88.4 443 172.16.94.227 443
ip nat inside source static tcp 172.16.88.4 53 172.16.94.226 53
ip nat inside source static udp 172.16.88.4 53 172.16.94.226 53
int f0/0.170
ip nat outside
int f0/0.186
ip nat inside
int f0/0.187
ip nat inside
int f0/0.188
ip nat inside
int f0/0.189
ip nat inside
int f0/0.190
ip nat inside

RCOMP 2021-2022 Project - Sprint 3 - Member 1200884 folder
===========================================

Firstly, in order to start the dynamic routing it was necessary using the command to erase the static routing, and afterwards, creating the networks with each Vlan's ip and the area associated with the building.
Then it was necessary to have a new server connected to the building router which was already established, althoug the first should be in a different floor. One server is directed to the html page.
The third part it was needed to include option 150 in the voip vlan which was done with the command option 150 ip x.x.x.x.
The ephone was also configured in order to establish a connection between the phones with the following set of commands
telephony-service
	no auto-reg-ephone
	max-ephones 10
	max-dn 10
	ip source-address 172.16.93.225 port 2000
	auto assign 1 to 2(esta linha nao sei se esta bem)
ephone-dn 1
	number 10001
ephone 1
	type 7960
	mac-address 00D0.976D.1BC1 (por o numero de um dos vossos telefones)
	button 1:1 (associar a linha 1 do ephone 1 com o ephone-dn 1)
ephone-dn 2
	number 10002
ephone 2
	type 7960
	mac-address 00D0.976D.AB12 (por o numero de um dos vossos telefones)
	button 1:2 (associar a linha 1 do ephone 2 com o ephone-dn 2)
dial-peer voice 
Afterwards, to configure the DNS in the network, the following commands were used ip dhcp pool NET-1-171 
dns-server server ip
exit
ip dhcp pool NET-1-172 
dns-server server ip
exit
ip dhcp pool NET-1-173 
dns-server server ip
exit
ip dhcp pool VOICE-1 
dns-server server ip
exit
Lastly, to the network address translation the following set of commands was used to configure the default service ports as 80 and 443
int f0/0.170
ip nat outside
int f0/0.181
ip nat inside
int f0/0.182
ip nat inside
int f0/0.183
ip nat inside
int f0/0.184
ip nat inside
int f0/0.185
ip nat inside

dial-peer voice 1 voip
destination-pattern 1....
session target ipv4:172.16.90.66

dial-peer voice 2 voip
destination-pattern 2....
session target ipv4:172.16.92.241
dial-peer voice 4 voip
destination-pattern 4....
session target ipv4:172.16.94.241

dial-peer voice 5 voip
destination-pattern 5....
session target ipv4:172.16.95.225
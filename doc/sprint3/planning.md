RCOMP 2021-2022 Project - Sprint 3 planning
===========================================
### Sprint master: 1200829 ###
# 1. Sprint's backlog #
In this sprint, each team member will keep working on the same network simulation from the previous
sprint (concerning the same building). From the already established layer three configurations, now,
OSPF based dynamic routing will be used to replace the previously applied static routing.

# 2. Technical decisions and coordination #
  * The ospf area id is equal to the building number. Area 0 is the bacbone area
  * VoIP phone numbers of each building are: x000n. x is the number of the building and n is 1 and 2.
# 3. Subtasks assignment #
Each team member was in charge of the same building as last sprint


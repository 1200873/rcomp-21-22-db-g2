RCOMP 2021-2022 Project - Sprint 3 review
=========================================
### Sprint master: 1200829 ###
# 1. Sprint's backlog #
In this sprint, each team member will keep working on the same network simulation from the previous
sprint (concerning the same building). From the already established layer three configurations, now,
OSPF based dynamic routing will be used to replace the previously applied static routing.
# 2. Subtasks assessment #
One by one, each team member presents his/her outcomes to the team, the team assesses 		the accomplishment of the subtask backlog.
The subtask backlog accomplishment can be assessed as one of:

  * Totally implemented with no issues
  * Totally implemented with issues
  * Partially implemented with no issues
  * Partially implemented with issues
## 2.1. 1200876 - Building 1#
### Partially implemented with issues. ###
We havent finished firewall
## 2.2. 1200829 - Building 2#
### Partially implemented with issues. ###
We havent finished firewall and my VoiP phones have no Gateway
## 2.3. 1200884 - Building 3 #
### Partially implemented with issues. ###
We havent finished firewall 
## 2.4. 1200873 - Building 4 #
### Partially implemented with issues. ###
We havent finished firewall and my NAT wasn't tested
## 2.5. 1210829 - Building 5 #
### Partially implemented with issues. ###
We havent finished firewall and my VoiP phones have no Gateway
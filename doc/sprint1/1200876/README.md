RCOMP 2021-2022 Project - Sprint 1 - Member 1200876 folder

===========================================

(This folder is to be created/edited by the team member 1200876 only)


![Piso 0 Outlets](Salas 1.0.png)

![Piso 1 Outlets](Salas 1.1.png)


Nos documentos apresentados acima é possivel verificar a estruturação decidia para a colocação de todos os outlets. Para tal, foram seguidas varias normas, tais como:



* A colocação de 2 outlets por cada 10m^2 


* A representação de onde será posta calha técnica e , no caso do piso 1, esteira técnica de maneira a fazer chegar todos os cabos às devidas salas.


* A verificação de que nenhum cabo ultrapassa os 90 metros foi feita e pode ser consultada no documento de informação adicional.



Na estruturação feita, decidi colocar dois bastidor no piso 0 com um HC a servir ambos os pisos e o outro a servir de MC para o resto dos edifícios.


Este bastidor contém 3 patch panels de cobre(todos de 48 portas), pois foi necessário deixar algum espaço para mais alguns cabos caso necessário. Coloquei também 3 switch de cobre de 48 portas para receberem todods os cabos de cobre vindos dos patch panels. Destes switch são enviados 6 cabos de fibra ótica para um patch panel de fibra de 24 portas (2 cabos de cada switch), que consequentemente, os envia para o switch de fibra de 20 portas(IC). 



Para escolher o tamanho deste bastidor é preciso calcular o tamanho em U de todos este equipamentos juntos:


* 3 patch panel de cobre de 48 portas(2U cada) = 6U


* 3 switch de cobre de 48 portas(2U cada) = 6U


* 2 patch panel de fibra de 24 portas = 2U


* 1 switch de fibra de 20 portas= 1U


* Total:15U 


Tendo estes calculos feitos e deixando espaço entre equipamentos e também para possiveis futuras mudanças foi escolhido um bastidor de 20U.


Em suma,o inventário final do edifício 1 é:


* 1 bastidor de 20U.


* 3 patch panel de cobre de 48 portas


* 3 switch de cobre de 48 portas


* 2 patch panel de fibra de 24 portas


* 1 switch de fibra de 20 portas


* 4957,1 metros de Cabo CAT6


* 128 Outlets


* 9 metros de Fibra ótica


Estes cálculos podem ser verificados no documento com toda a informação de cada outlet individual e agrupados em salas:

![Piso 0 Outlets](Tabela Cabos geral.png)

![Piso 1 Outlets](Tabela Cabos por outlet.png)